package com.btmatthews.todo.server.repository;

import com.btmatthews.todo.server.domain.Owner;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface OwnerRepository extends ReactiveMongoRepository<Owner, String> {
}
