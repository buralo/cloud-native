package com.btmatthews.todo.server.controller;

import com.btmatthews.todo.server.domain.Owner;
import com.btmatthews.todo.server.domain.TodoItem;
import com.btmatthews.todo.server.payload.TodoItemRequest;
import com.btmatthews.todo.server.repository.OwnerRepository;
import com.btmatthews.todo.server.repository.TodoItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@RestController
public class TodoItemRestController {

    private final OwnerRepository ownerRepository;

    private final TodoItemRepository todoItemRepository;

    public TodoItemRestController(final OwnerRepository ownerRepository,
                                  final TodoItemRepository todoItemRepository) {
        this.ownerRepository = ownerRepository;
        this.todoItemRepository = todoItemRepository;
    }

    @GetMapping("/api/owners/{owner}/items")
    public Flux<TodoItem> listItems(
            @PathVariable("owner") final String owner,
            @RequestParam(value = "start", defaultValue = "0") final int start,
            @RequestParam(value = "limit", defaultValue = "10000") final int limit,
            @RequestParam(value = "order", defaultValue = "-dueDate") final List<String> orders) {
        return todoItemRepository.findByOwner(owner);
    }

    @PostMapping("/api/owners/{owner}/items")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<TodoItem> createItem(@PathVariable("owner") final String ownerId,
                                     @RequestBody final TodoItemRequest request) {
        return ownerRepository.findById(ownerId)
                .flatMap(owner -> createItem(owner, request));
    }

    @GetMapping("/api/owners/{owner}/items/{item}")
    public Mono<TodoItem> getItem(@PathVariable("owner") final String owner,
                                  @PathVariable("item") final String item) {
        return todoItemRepository.findByOwnerAndId(owner, item);
    }

    @PutMapping("/api/owners/{owner}/items/{item}")
    public Mono<TodoItem> updateItem(@PathVariable("owner") final String ownerId,
                                     @PathVariable("item") final String item,
                                     @RequestBody final TodoItemRequest request) {
        return ownerRepository.findById(ownerId)
                .flatMap(owner -> updateItem(owner, item, request));
    }

    @DeleteMapping("/api/owners/{owner}/items/{item}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> deleteItem(@PathVariable("owner") final String ownerId,
                                 @PathVariable("item") final String item) {
        return ownerRepository.findById(ownerId)
                .flatMap(owner -> deleteItem(owner, item));
    }

    private Mono<TodoItem> createItem(final Owner owner,
                                      final TodoItemRequest request) {
        final TodoItem todoItem = new TodoItem(
                UUID.randomUUID().toString(),
                owner.getId(),
                request.getTask(),
                request.getPriority(),
                request.getDueDate());
        return todoItemRepository.insert(todoItem);
    }

    private Mono<TodoItem> updateItem(final Owner owner,
                                      final String itemId,
                                      final TodoItemRequest request) {
        return todoItemRepository.findByOwnerAndId(owner.getId(), itemId)
                .map(item -> new TodoItem(
                        item.getId(),
                        owner.getId(),
                        request.getTask(),
                        request.getPriority(),
                        request.getDueDate(),
                        item.getStatus(),
                        item.getCompletionDate()))
                .flatMap(todoItemRepository::save);
    }

    private Mono<Void> deleteItem(final Owner owner,
                                  final String item) {
        return todoItemRepository.deleteByOwnerAndId(owner.getId(), item);
    }
}
