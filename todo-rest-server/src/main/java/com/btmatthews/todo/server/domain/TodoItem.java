package com.btmatthews.todo.server.domain;

import java.time.ZonedDateTime;

public class TodoItem {

    private String id;

    private String task;

    private String owner;

    private TodoItemPriority priority;

    private ZonedDateTime dueDate;

    private TodoItemStatus status;

    private ZonedDateTime completionDate;

    public TodoItem() {
    }

    public TodoItem(final String id,
             final String owner,
             final String task,
             final TodoItemPriority priority,
             final ZonedDateTime dueDate) {
        this(id, owner, task, priority, dueDate, TodoItemStatus.OPEN, null);
    }

    public TodoItem(final String id,
                    final String owner,
                    final String task,
                    final TodoItemPriority priority,
                    final ZonedDateTime dueDate,
                    final TodoItemStatus status,
                    final ZonedDateTime completionDate) {
        this.id = id;
        this.owner = owner;
        this.task = task;
        this.priority = priority;
        this.dueDate = dueDate;
        this.status = status;
        this.completionDate = completionDate;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(final String task) {
        this.task = task;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(final String owner) {
        this.owner = owner;
    }

    public TodoItemPriority getPriority() {
        return priority;
    }

    public void setPriority(final TodoItemPriority priority) {
        this.priority = priority;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(final ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public TodoItemStatus getStatus() {
        return status;
    }

    public void setStatus(final TodoItemStatus status) {
        this.status = status;
    }

    public ZonedDateTime getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(final ZonedDateTime completionDate) {
        this.completionDate = completionDate;
    }
}
