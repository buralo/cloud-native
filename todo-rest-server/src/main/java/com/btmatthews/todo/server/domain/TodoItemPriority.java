package com.btmatthews.todo.server.domain;

public enum TodoItemPriority {
    HIGH,
    MEDIUM,
    LOW
}
