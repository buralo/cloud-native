package com.btmatthews.todo.server.controller;

import com.btmatthews.todo.server.domain.Owner;
import com.btmatthews.todo.server.payload.OwnerRequest;
import com.btmatthews.todo.server.repository.OwnerRepository;
import com.btmatthews.todo.server.repository.TodoItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@RestController
public class OwnerRestController {

    private final OwnerRepository ownerRepository;

    private final TodoItemRepository todoItemRepository;

    public OwnerRestController(final OwnerRepository ownerRepository,
                               final TodoItemRepository todoItemRepository) {
        this.ownerRepository = ownerRepository;
        this.todoItemRepository = todoItemRepository;
    }

    @GetMapping("/api/owners")
    public Flux<Owner> listOwners(@RequestParam(value = "start", defaultValue = "0") final int start,
                                  @RequestParam(value = "limit", defaultValue = "10000") final int limit,
                                  @RequestParam(value = "order", defaultValue = "+order") final List<String> order) {
        return ownerRepository.findAll();
    }

    @PostMapping("/api/owners")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Owner> createOwner(@RequestBody final OwnerRequest request) {
        final Owner owner = new Owner();
        owner.setId(UUID.randomUUID().toString());
        owner.setName(request.getName());
        return ownerRepository.insert(owner);
    }

    @GetMapping("/api/owners/{owner}")
    public Mono<Owner> getOwner(@PathVariable("owner") final String owner) {
        return ownerRepository.findById(owner);
    }

    @PutMapping("/api/owners/{owner}")
    public Mono<Owner> updateOwner(@PathVariable("owner") final String owner,
                                   @RequestBody final OwnerRequest request) {
        return ownerRepository.findById(owner)
                .map(o -> new Owner(o.getId(), request.getName()))
                .flatMap(ownerRepository::save);
    }

    @DeleteMapping("/api/owners/{owner}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> deleteOwner(@PathVariable("owner") final String owner) {
        return todoItemRepository
                .deleteByOwner(owner)
                .then(ownerRepository.deleteById(owner));
    }
}
