package com.btmatthews.todo.server.payload;

import com.btmatthews.todo.server.domain.TodoItemPriority;

import java.time.ZonedDateTime;

public class TodoItemRequest {

    private String task;

    private TodoItemPriority priority;

    private ZonedDateTime dueDate;

    public String getTask() {
        return task;
    }

    public void setTask(final String task) {
        this.task = task;
    }

    public TodoItemPriority getPriority() {
        return priority;
    }

    public void setPriority(final TodoItemPriority priority) {
        this.priority = priority;
    }

    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(final ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }
}
