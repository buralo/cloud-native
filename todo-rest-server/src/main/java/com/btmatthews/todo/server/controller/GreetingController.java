package com.btmatthews.todo.server.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class GreetingController {

    private final String greeting;

    public GreetingController(@Value("${greeting}") final String greeting) {
        this.greeting = greeting;
    }

    @RequestMapping("/greeting")
    public Mono<String> greeting() {
        return Mono.just(greeting);
    }
}
