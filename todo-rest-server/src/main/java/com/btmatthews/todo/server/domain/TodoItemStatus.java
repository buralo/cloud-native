package com.btmatthews.todo.server.domain;

public enum TodoItemStatus {
    OPEN,
    STARTED,
    COMPLETED
}
