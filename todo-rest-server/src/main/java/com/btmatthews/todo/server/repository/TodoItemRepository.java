package com.btmatthews.todo.server.repository;

import com.btmatthews.todo.server.domain.TodoItem;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TodoItemRepository extends ReactiveMongoRepository<TodoItem, String> {

    Flux<TodoItem> findByOwner(String owner);

    Mono<TodoItem> findByOwnerAndId(String owner, String id);

    Mono<Void> deleteByOwner(String owner);

    Mono<Void> deleteByOwnerAndId(String owner, String id);
}
