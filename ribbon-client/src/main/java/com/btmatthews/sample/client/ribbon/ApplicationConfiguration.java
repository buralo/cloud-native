package com.btmatthews.sample.client.ribbon;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.*;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class ApplicationConfiguration {

    @Bean
    public IPing ribbonPing(final IClientConfig config) {
        return new PingUrl(false, "/actuator/health");
    }

    @Bean
    public IRule ribbonRule(final IClientConfig config) {
        return new AvailabilityFilteringRule();
    }
/*
    @Bean
    public IPingStrategy parallelPing() {
        return (ping, servers) -> {
            if (servers.length == 0) {
                return new boolean[0];
            }
            if (servers.length == 1) {
                return new boolean[]{ping.isAlive(servers[0])};
            }
            final ExecutorService executorService = Executors.newFixedThreadPool(5);
            final boolean[] results = new boolean[servers.length];
            IntStream.range(0, servers.length)
                    .forEach(i -> {
                        results[i] = false;
                        executorService.submit(() -> results[i] = ping.isAlive(servers[i]));
                    });
            executorService.shutdown();
            return results;
        };
    }*/
}
