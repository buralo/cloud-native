package com.btmatthews.sample.client.ribbon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
public class ApplicationCLI implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationCLI.class);

    private final RestTemplate restTemplate;

    public ApplicationCLI(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void run(final String... args) throws Exception {
        String greeting = getGreeting();
        while (greeting != null) {
            LOGGER.info(greeting);
            Thread.sleep(500);
            greeting = getGreeting();
        }
    }

    public String getGreeting() {
        try {
            return restTemplate.getForObject("http://greeting/greeting", String.class);
        } catch (final ResourceAccessException e) {
            return "Burp";
        }
    }
}
