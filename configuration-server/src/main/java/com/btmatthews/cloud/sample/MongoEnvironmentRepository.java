package com.btmatthews.cloud.sample;

import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Function;

@Repository
public class MongoEnvironmentRepository implements EnvironmentRepository {

    private static final String LABEL = "label";
    private static final String PROFILE = "profile";
    private static final String DEFAULT = "default";
    private static final String DEFAULT_PROFILE = null;
    private static final String DEFAULT_LABEL = null;

    private MongoTemplate mongoTemplate;
    private MapFlattener mapFlattener;

    public MongoEnvironmentRepository(final MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
        this.mapFlattener = new MapFlattener();
    }

    @Override
    public Environment findOne(final String name,
                               final String profile,
                               final String label) {
        final String[] profilesArr = StringUtils.commaDelimitedListToStringArray(profile);
        List<String> profiles = new ArrayList<>(Arrays.asList(profilesArr));
        for (int i = 0; i < profiles.size(); i++) {
            if (DEFAULT.equals(profiles.get(i))) {
                profiles.set(i, DEFAULT_PROFILE);
            }
        }
        profiles.add(DEFAULT_PROFILE);
        profiles = sortedUnique(profiles);

        final List<String> labels = Arrays.asList(label, DEFAULT_LABEL); // Default configuration will have 'null' label

        final Query query = new Query();
        query.addCriteria(Criteria.where(PROFILE).in(profiles.toArray()));
        query.addCriteria(Criteria.where(LABEL).in(labels.toArray()));

        Environment environment;
        try {
            final List<MongoPropertySource> sources = mongoTemplate.find(query, MongoPropertySource.class, name);
            sort(sources, labels, MongoPropertySource::getLabel);
            sort(sources, profiles, MongoPropertySource::getProfile);
            environment = new Environment(name, profilesArr, label, null, null);
            for (final MongoPropertySource propertySource : sources) {
                final String sourceName = generateSourceName(name, propertySource);
                final Map<String, Object> flatSource = mapFlattener.flatten(propertySource.getSource());
                final PropertySource propSource = new PropertySource(sourceName, flatSource);
                environment.add(propSource);
            }
        } catch (Exception e) {
            throw new IllegalStateException("Cannot load environment", e);
        }

        return environment;
    }

    private ArrayList<String> sortedUnique(final List<String> values) {
        return new ArrayList<>(new LinkedHashSet<>(values));
    }

    private <T> void sort(final List<T> items,
                          final List<String> keys,
                          final Function<T, String> getKey) {
        items.sort((item1, item2) -> {
            final int i1 = keys.indexOf(getKey.apply(item1));
            final int i2 = keys.indexOf(getKey.apply(item2));
            return Integer.compare(i1, i2);
        });
    }

    private String generateSourceName(final String environmentName,
                                      final MongoPropertySource source) {
        final String profile = source.getProfile() != null ? source.getProfile() : DEFAULT;
        final String label = source.getLabel();
        if (label != null) {
            return String.format("%s-%s-%s", environmentName, profile, label);
        } else {
            return String.format("%s-%s", environmentName, profile);
        }
    }
}
