package com.btmatthews.cloud.sample;

import java.util.LinkedHashMap;
import java.util.Map;

public class MongoPropertySource {

    private String profile;
    private String label;
    private Map<String, Object> source = new LinkedHashMap<>();

    public String getProfile() {
        return profile;
    }

    public void setProfile(final String profile) {
        this.profile = profile;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public Map<String, Object> getSource() {
        return source;
    }

    public void setSource(final Map<String, Object> source) {
        this.source.clear();
        this.source.putAll(source);
    }

}