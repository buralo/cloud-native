package com.btmatthews.cloud.sample;

import org.springframework.beans.factory.config.YamlProcessor;

import java.util.Map;

class MapFlattener extends YamlProcessor {

    Map<String, Object> flatten(final Map<String, Object> source) {
        return getFlattenedMap(source);
    }
}
